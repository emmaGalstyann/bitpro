$(document).ready(function() {
  $('.play, .pause').on('click', function() {
    if ($(this).hasClass('play')) {
      $(this)
        .removeClass('play')
        .addClass('pause');
      $('#video')
        .get(0)
        .play();
    } else if ($(this).hasClass('pause')) {
      $(this)
        .removeClass('pause')
        .addClass('play');
      $('#video')
        .get(0)
        .pause();
    }
  });
  $('.video-section').mouseover(function() {
    $(this)
      .find('.pause')
      .addClass('visible');
  });
  $('.video-section').mouseout(function() {
    $(this)
      .find('.pause')
      .removeClass('visible');
  });

  /*$('.slider-nav').on('init', function(event, slick, direction) {
    $('.slick-slide').removeClass('next prev');
    $('.slick-center')
      .next()
      .addClass('next');
    $('.slick-center')
      .prev()
      .addClass('prev');
  });
  $('.slider-nav').on('afterChange', function(event, slick, direction) {
    $('.slick-slide').removeClass('next prev');
    $('.slick-center')
      .next()
      .addClass('next');
    $('.slick-center')
      .prev()
      .addClass('prev');
  });*/
  $('header .main-menu li a').on('click', function(event) {
    //отменяем стандартную обработку нажатия по ссылке
    event.preventDefault();
    $('.btn-menu').removeClass('active');
    $('.menu-block').removeClass('menu-block-open');
    $('body').removeClass('hidden');

    //забираем идентификатор бока с атрибута href
    var id = $(this).attr('href'),
      //узнаем высоту от начала страницы до блока на который ссылается якорь
      top = $(id).offset().top;

    //анимируем переход на расстояние - top за 1500 мс
    $('html, body')
      .stop()
      .animate(
        {
          scrollTop: top - 50,
          easing: 'easein',
        },
        1000
      );
  });

  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav',
  });
  $('.slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    centerMode: true,
    asNavFor: '.slider-for',
    arrows: true,
    dots: false,
    centerPadding: 0,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
        },
      },
    ],
  });

  $('.btn-menu').on('click', function() {
    $(this).toggleClass('active');
    $('body').toggleClass('hidden');
    $('.menu-block').toggleClass('menu-block-open');
  });

  $('.login-btn').each(function() {
    $(this).on('click', function(e) {
      e.preventDefault();
      $('.pop-up-overlay').toggleClass('active');
      $('body').toggleClass('active-pop-up');
    });
  });
  $('.close').on('click', function() {
    $('.pop-up-overlay').toggleClass('active');
    $('body').toggleClass('active-pop-up');
  });

  $('#question').validate({
    rules: {
      email: {
        email: true,
        required: true,
      },
    },
    submitHandler: function(form) {
      var data = {};
      $('.input-overlay')
        .children()
        .each(function() {
          data[$(this).attr('name')] = $(this).val();
          console.log($(this));
        });
      console.log(data);
      $.ajax({
        type: 'POST', //Метод отправки
        url: '../../send.php', //путь до php фаила отправителя
        data: data,
        success: function() {
          //код в этом блоке выполняется при успешной отправке сообщения
          alert('Ваше сообщение отправлено!');
        },
      });
    },
  });

  $('.lang').on('click', function(e) {
    e.preventDefault();
    $(this).toggleClass('active');
    $('.lang ul').slideToggle();
  });

  $('.lang ul li').on('click', function() {
    $('.lang-change ul li').removeClass('active-language');
    $(this).toggleClass('active-language');
    var lang = $('.active-language').attr('data-lang');

    $('.language-change').each(function() {
      $(this).text(translate[lang][$(this).attr('data-language')]);
      $('input.language-change').each(function() {
        $(this).attr(
          'placeholder',
          translate[lang][$(this).attr('data-language')]
        );
      });
    });
  });
  var translate = {
    ru: {
      lang: 'RUS',
    },
    en: {
      lang: 'ENG',
    },
    es: {
      lang: 'ESP',
    },
    it: {
      lang: 'ITA',
    },
    ch: {
      lang: '中文',
    },
    fr: {
      lang: 'FRA',
    },
  };

  $('.target').viewportChecker({
    classToAdd: 'load',
    offset: 170,
    callbackFunction: function(elem, action) {
      if ($(window).width() > 767) {
        var service = new TimelineMax();
        var textMove = new TimelineMax();

        service
          .set(
            $(
              '.first-service-overflow .platform,.first-service-overflow .line,.second-service-overflow .line,.second-service-overflow .line-hidden, .first-service-overflow .platforme-shadow, .second-service-overflow .platforme-overflow .platforme'
            ),
            { visibility: 'visible' }
          )
          .from(
            $('.first-service-overflow .platforme-overflow .platform'),
            0.5,
            {
              y: '100%',
              ease: Power1.easeOut,
            }
          )
          .fromTo(
            $('.first-service-overflow .platforme-shadow'),
            0.3,
            { scaleY: 0, scaleX: 0, transformOrigin: '50% 50%' },
            { scaleY: 1, scaleX: 1, ease: Power0.easeInOut }
          )
          .staggerFromTo(
            $('.first-service-overflow .notebook'),
            0.5,
            { y: -20, transformOrigin: 'center center' },
            { y: 0, ease: Power1.easeInOut, visibility: 'visible' },
            0.4
          )
          .fromTo(
            $('.first-service-overflow .shadow-money'),
            0.2,
            { autoAlpha: 0 },
            { autoAlpha: 1, ease: Power1.easeInOut },
            '-=0.2'
          )
          .staggerFromTo(
            $('.first-service-overflow .point'),
            1,
            { y: -40, transformOrigin: 'center center' },
            { y: 0, ease: Power1.easeInOut, visibility: 'visible' },
            0.05
          )
          .staggerFromTo(
            $('.first-service-overflow .shadow-point'),
            0.3,
            { autoAlpha: 0 },
            { autoAlpha: 1, ease: Power1.easeInOut },
            0.5,
            '-=1.2'
          )
          .fromTo(
            $('.women .men'),
            0.5,
            { autoAlpha: 0 },
            { autoAlpha: 1, ease: Power1.easeInOut },
            '-=0.3'
          )
          .staggerFromTo(
            $('.man'),
            0.5,
            { autoAlpha: 0 },
            { autoAlpha: 1, ease: Power1.easeInOut },
            0.2,
            '-=0.3'
          )
          .staggerFromTo(
            $('.first-service-overflow .man-shadow'),
            0.3,
            { autoAlpha: 0 },
            { autoAlpha: 1, ease: Power1.easeInOut },
            0.5,
            '-=0.8'
          )
          .staggerFrom(
            $('.first-service-overflow .line'),
            0.5,
            { drawSVG: 0 },
            0.5,
            '-=3.5'
          )
          .fromTo(
            $('.logo-gerb-overlay .logo-center'),
            0.5,
            { scaleY: 0, scaleX: 0, transformOrigin: '50% 50%' },
            {
              scaleY: 1,
              scaleX: 1,
              ease: Power0.easeInOut,
              visibility: 'visible',
            },
            '-=3'
          )
          .from(
            $('.second-service-overflow .line-hidden'),
            0.5,
            {
              drawSVG: 0,
            },
            '-=2.5'
          )
          .from(
            $('.second-service-overflow .platforme-overflow .platforme'),
            0.5,
            {
              y: '120%',
              ease: Power1.easeOut,
            },
            '-=2'
          )
          .fromTo(
            $('.second-service-overflow .platforme-shadow'),
            0.3,
            { scaleY: 0, scaleX: 0, transformOrigin: '50% 50%' },
            {
              scaleY: 1,
              scaleX: 1,
              ease: Power0.easeInOut,
              visibility: 'visible',
            },
            '-=1.5'
          )
          .fromTo(
            $('.second-service-overflow .center-change'),
            0.3,
            { autoAlpha: 0 },
            { autoAlpha: 1, ease: Power1.easeInOut },
            '-=1.2'
          )
          .from($('.second-service-overflow .line'), 0.5, { drawSVG: 0 })
          .fromTo(
            $('.second-service-overflow .men'),
            0.5,
            { autoAlpha: 0 },
            { autoAlpha: 1, ease: Power1.easeInOut },
            '-=0.7'
          )
          .staggerFromTo(
            $('.second-service-overflow .shadow-men'),
            0.3,
            { autoAlpha: 0 },
            { autoAlpha: 1, ease: Power1.easeInOut },
            0.5,
            '-=1.5'
          )
          .staggerFromTo(
            $('.second-service-overflow .computer'),
            0.5,
            { y: -20, transformOrigin: 'center center' },
            {
              y: 0,
              ease: Power1.easeInOut,
              visibility: 'visible',
              onComplete: function() {
                textMove
                  .fromTo(
                    $('.move-text'),
                    0.5,
                    { visibility: 'hidden' },
                    { visibility: 'visible' }
                  )
                  .fromTo(
                    $('.move-text-two'),
                    0.5,
                    { visibility: 'hidden' },
                    { visibility: 'visible' }
                  );
              },
            },
            0.4,
            '-=1'
          )
          .staggerFromTo(
            $('.second-service-overflow .point'),
            1,
            { y: -40, transformOrigin: 'center center' },
            { y: 0, ease: Power1.easeInOut, visibility: 'visible' },
            0.05,
            '-=1.2'
          )
          .staggerFromTo(
            $('.second-service-overflow .shadow-point'),
            0.3,
            { autoAlpha: 0 },
            { autoAlpha: 1, ease: Power1.easeInOut },
            0.5,
            '-=1.2'
          );
      }
    },
  });
  $('.advantages').viewportChecker({
    classToAdd: 'load',
    offset: 150,
    callbackFunction: function(elem, action) {
      if ($(window).width() > 767) {
        var advantages = new TimelineMax();
        advantages
          .set(
            $(
              '.advantages-overlay .platforme-shadow, .advantages-overlay .line '
            ),
            {
              visibility: 'visible',
            }
          )
          .fromTo(
            $('.advantages-overlay .gerb'),
            0.4,
            { autoAlpha: 0 },
            { autoAlpha: 1, ease: Power1.easeInOut }
          )
          .staggerFrom($('.advantages-overlay .line'), 0.5, { drawSVG: 0 }, 0.1)
          .staggerFromTo(
            $('.advantages-overlay .platforme'),
            0.6,
            { scaleY: 0, scaleX: 0, transformOrigin: '50% 50%' },
            {
              scaleY: 1,
              scaleX: 1,
              ease: Power0.easeInOut,
              visibility: 'visible',
            },
            0.1
          )

          .staggerFromTo(
            $('.advantages-overlay .platforme-shadow'),
            0.2,
            { autoAlpha: 0 },
            { autoAlpha: 0.2, ease: Power1.easeInOut }
          )
          .staggerFromTo(
            $('.advantages-overlay .advantages-elem'),
            1.2,
            { y: -20, transformOrigin: 'center center' },
            {
              y: 0,
              ease: Power1.easeInOut,
              visibility: 'visible',
              onComplete: function() {
                $('.advantages-overlay .advantages-elem').addClass('move');
              },
            },
            0.2
          )
          .staggerFromTo(
            $('.text-opacity'),
            0.6,
            { autoAlpha: 0 },
            { autoAlpha: 1, ease: Power1.easeInOut },
            0.1,
            '-=2'
          );
      }
    },
  });
});
/*
setTimeout(function() {
  var v = document.getElementsByTagName('video')[0];
  v.addEventListener(
    'click',
    function() {
      document.getElementsByTagName('video')[0].play();
    },
    true
  );
  v.currentTime = 0.1;
}, 500);
*/
